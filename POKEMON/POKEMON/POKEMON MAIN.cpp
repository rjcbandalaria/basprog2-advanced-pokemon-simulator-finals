#include<iostream>
#include<string>
#include<math.h>
#include<vector>
#include<time.h>
#include"Movement.h"
#include"PokeCenter.h"
#include"Pokemon.h"
#include"PokemonTrainer.h"
#include"ProfessorOak.h"
#include"Map.h"

using namespace std; 

void startScreen() {
	cout << R"(.______     ______    __  ___  _______ .___  ___.   ______   .__   __.                          
		| _  \ / __  \  |  |/  / |   ____ || \/   |  /  __  \ | \ |  |
		|  |_)  | |  |  |  | |  '  /  |  |__   |  \  /  | |  |  |  | |   \|  |                          
		| ___/  |  |  |  | | < | __|  |  |\/|  | |  |  |  | |  . `  |
		|  |      |  `- - '  | |  .  \  |  |____ |  |  |  | |  `--'  | |  |\   |
		| _ | \______/  |__ | \__\ | _______ || __|  |__ | \______/  |__ | \__ |

		_______.__.___  ___.__    __   __          ___.___________.______.______
		/       ||  | |   \/   | |  |  |  | |  |        /   \  |           | /  __  \ | _  \
		| (----`|  | |  \  /  | |  |  |  | |  |       / ^ \ `---|  |----`|  |  |  | |  |_) |
		\   \    |  | |  |\/|  | |  |  |  | |  |      /  /_\  \    |  |     |  |  |  | |      /
		.----)   |   |  | |  |  |  | |  `- - '  | |  `----./  _____  \   |  |     |  `--'  | |  |\  \----.
		| _______/    |__| |__|  |__ | \______/  |_______ / __ / \__\ | __ | \______/  | _ | `._____ |
		)" << endl << endl;
	cout << "       BANDALARIA, Ronald Jon Christopher C.             " << endl;
	cout << "                    BSIEMC                               " << endl;
	system("pause");
	system("cls");

}

void chooseDirection(char& direction) {

	cout << "W to walk North " << endl;
	cout << "A to walk West" << endl;
	cout << "S to walk South" << endl;
	cout << "D to walk East" << endl;

	cin >> direction;

}
void chooseTrainerAction(PokeTrainer* trainer, int& choice) {
	if (trainer->isTrainerSafe == true) {
		cout << "Choose next Action" << endl;
		cout << "1 to Move | 2 to Check Pokemon Stats | 3 to Pokemon Center" << endl;
	}
	else if (trainer->isTrainerSafe == false) {
		cout << "Choose next Action" << endl;
		cout << "1 to Move | 2 to Check Pokemon Stats " << endl;
	}
	 cin >> choice;
}
void encounterMenu(int& choice) {
	cout << "Action Menu" << endl;
	cout << "1 | Attack    2 | Catch   3 | Run " << endl;
	cin >> choice;
	system("cls");

}
void deletePokemon(Pokemon* pokemon){
	delete pokemon;
	pokemon = NULL;
}
void deleteProfessor(ProfessorOak* oak) {
	delete oak;
	oak = NULL;
}
void deleteMap(Map* map) {
	delete map;
	map = NULL;
}
void deleteTrainer(PokeTrainer* trainer) {
	delete trainer;
	trainer = NULL;
}
void deletePokeCenter(PokeCenter* pokeCenter) {
	delete pokeCenter;
	pokeCenter = NULL; 
}
void battlePokemon(Pokemon* trainer, Pokemon* wildPokemon) {
	int attackRate = 80;
	int attackChance;
	attackChance = (rand() % 100)+1;

if (attackChance <= attackRate) {
	trainer->attackPokemon(wildPokemon);
}
else if (attackChance > attackRate) {
	cout << trainer->name << " missed" << endl;
}
attackChance = (rand() % 100) + 1;
if (attackChance <= attackRate) {
	wildPokemon->attackPokemon(trainer);
}
else if (attackChance > attackRate) {
	cout << wildPokemon->name << " missed" << endl;
}

cout << "Trainer Pokemon Stats" << endl;
trainer->displayStats();
cout << endl;
cout << "Wild Pokemon Stats" << endl;
wildPokemon->displayStats();
system("pause");
system("cls");

}
void catchPokemon(PokeTrainer* trainer, Pokemon* wildPokemon) {
	int captureRate = 30;
	int captureChance;
	cout << "You threw a pokeball" << endl;
	cout << "." << endl;
	system("pause");
	cout << ". . " << endl;
	system("pause");
	cout << ". . . " << endl;
	system("pause");
	captureChance = (rand() % 100) + 1;
	if (captureChance <= captureRate) {
		trainer->getPokemon(wildPokemon);
		cout << "You captured the wild Pokemon" << endl;
		wildPokemon->isCaptured = true;
		system("pause");
		system("cls");
	}
	else if (captureChance >= captureRate) {
		cout << wildPokemon->name << " broke free" << endl;
		wildPokemon->isCaptured = false;
		system("pause");
		system("cls");
	}
}
void encounterScenario(PokeTrainer* trainer, Map* map) {

	int trainerChoice = 0;
	Pokemon* wildPokemon = map->generateWildPokemon();

	cout << "You have encountered a Wild Pokemon" << endl;
	wildPokemon->displayStats();

	Pokemon* chosenPokemon = trainer->chooseBattlePokemon();

	while (wildPokemon->hp > 0) {
		cout << "Trainer Pokemon: " << chosenPokemon->name << endl;
		cout << "Wild Pokemon: " << wildPokemon->name << endl;
		encounterMenu(trainerChoice);
		if (trainerChoice == 1) {
			battlePokemon(chosenPokemon, wildPokemon);
			if (chosenPokemon->isDead()) {
				cout << "Your Pokemon fainted" << endl;
				chosenPokemon = trainer->chooseBattlePokemon();
				if (chosenPokemon->isDead()) {
					cout << "No more pokemon left to fight" << endl;
				}
			}
			else if (wildPokemon->isDead()) {
				cout << "Wild Pokemon fainted" << endl;
				chosenPokemon->gainExp();
				break;
			}
		}
		else if (trainerChoice == 2) {
			if (trainer->myPokemon->size() < 6) {
				catchPokemon(trainer, wildPokemon);
				if (wildPokemon->isCaptured == true) {
					break;
				}
			}
			else if (trainer->myPokemon->size() >= 6) {
				cout << "Your pocket is full" << endl;
				system("pause");
				system("cls");
			}
		}
		else if (trainerChoice == 3) {
			cout << "You fled" << endl;
			system("pause");
			system("cls");
			break;
		}
	}
	/*deletePokemon(chosenPokemon);
	deletePokemon(wildPokemon);*/
}

int main() {
	srand(time(NULL));
	PokeTrainer* trainer= new PokeTrainer();
	ProfessorOak* oak = new ProfessorOak();
	Map* map = new Map();
	PokeCenter* pokeCenter = new PokeCenter(); 
	int choice;
	char direction;

	oak->askTrainerName(trainer);
	oak->giveStarterPokemon(trainer);
	trainer->displayStats();
	deleteProfessor(oak);

	while (true) {
		map->determineLocation(trainer);
		map->displayPlayerCoordinates(trainer);
		if (trainer->isTrainerSafe == true) {
			chooseTrainerAction(trainer, choice);

			switch (choice) {
			case 1:
				chooseDirection(direction);
				trainer->playerMove(direction);
				break;
			case 2:
				trainer->checkPokemonStats();
				break;
			case 3:
				pokeCenter->healPokemon(trainer);
				break;
			}
		}
		else if (trainer->isTrainerSafe == false) {
			if (map->wildPokemonEncounter() == true) {
				encounterScenario(trainer, map);
			}
			else if (map->wildPokemonEncounter() == false) {
				chooseTrainerAction(trainer, choice);
				switch (choice) {
				case 1:
					chooseDirection(direction);
					trainer->playerMove(direction);
					break;
				case 2:
					trainer->checkPokemonStats();
					break;
				}
			}
		}
	}
	deleteMap(map);
	deletePokeCenter(pokeCenter);
	deleteTrainer(trainer);
	system("pause");
}