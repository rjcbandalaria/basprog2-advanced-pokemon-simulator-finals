#include "Map.h"
#include "Pokemon.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

Map::Map()
{
	srand(time(NULL));
	this->x = 0;
	this->y = 0;
	this->isSafe = true;

}

bool Map::wildPokemonEncounter()
{
	int chance;
	chance = (rand() % 100)+1;
	if (chance > 70) {
		return false;
	}
	else if (chance <= 70) {
		return true;
	}

}

Pokemon* Map::generateWildPokemon()
{
	int randomPokemon = (rand() % 15) + 1;
	switch (randomPokemon) {
	case 1: return new Pokemon("Rattata", 70, 5);
	case 2: return new Pokemon("Raticate", 95 , 6);
	case 3: return new Pokemon("Weedle", 50, 7);
	case 4: return new Pokemon("Butterfree", 80 , 5);
	case 5: return new Pokemon("Kakuna", 90, 5);
	case 6: return new Pokemon("Metapod", 100, 2);
	case 7: return new Pokemon("Pikachu", 85, 10);
	case 8: return new Pokemon("Spearow", 90, 8);
	case 9: return new Pokemon("Nidoran", 85, 9);
	case 10: return new Pokemon("Clefairy", 90, 6);
	case 11: return new Pokemon("Diglett", 80, 8);
	case 12: return new Pokemon("Machop", 90, 10);
	case 13: return new Pokemon("Geodude", 95, 8);
	case 14: return new Pokemon("Alakazam", 60, 10);
	case 15: return new Pokemon("Pidgey", 50, 5);
	}
}

bool Map::isSafeLocation()
{
	return isSafe;
}

void Map::determineLocation(PokeTrainer* trainer)
{
	if (trainer->playerPosition.x <= 1 && trainer->playerPosition.x >= -1 && trainer->playerPosition.y <= 1 && trainer->playerPosition.y >= -1) {
		cout << "You are in Pallet Town" << endl;
		trainer->isTrainerSafe = true;
	}

	else if (trainer->playerPosition.y == 2 || trainer->playerPosition.y == 3 && trainer->playerPosition.x >= -2 && trainer->playerPosition.x <= 2) {
		cout << "You are in Route 1" << endl;
		trainer->isTrainerSafe = false;

	}
	else if (trainer->playerPosition.y == 4 || trainer->playerPosition.y == 5 && trainer->playerPosition.x >= -1 && trainer->playerPosition.x <= 1) {
		cout << "Welcome to Viridian City" << endl;
		trainer->isTrainerSafe = true;
	}
	else {
		cout << "Unknown Location" << endl;
		cout << "Turn Back" << endl;
	}
	
}

void Map::displayPlayerCoordinates(PokeTrainer* trainer)
{
	cout << "(" <<trainer->playerPosition.x << " , " << trainer->playerPosition.y << ")" << endl;
}

Map::~Map()
{
	delete wildPokemon;
	wildPokemon = NULL;
}


