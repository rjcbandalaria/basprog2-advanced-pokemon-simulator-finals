#pragma once
#include<iostream>
#include<string>
#include<vector>
#include"Pokemon.h"
#include<time.h>
#include"PokemonTrainer.h"

using namespace std;

class PokeDex {
public:

	PokeDex();
	void displayList(); 
	bool randomEncounter();
	void randomEncounterPokemon();
	

	vector<Pokemon*>* wildPokemon = new vector<Pokemon*>();

};