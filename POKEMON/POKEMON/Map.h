#pragma once

#include<iostream>
#include<string>
#include"PokemonTrainer.h"
#include"Pokemon.h"

using namespace std; 

class Map {

public: 

	Map(); 
	bool wildPokemonEncounter();
	Pokemon* generateWildPokemon();
	bool isSafeLocation();
	void determineLocation(PokeTrainer* trainer);
	void displayPlayerCoordinates(PokeTrainer* trainer);
	~Map();

	



private: 
	
	int x; 
	int y;
	bool isSafe;
	Pokemon* wildPokemon = new Pokemon();

};