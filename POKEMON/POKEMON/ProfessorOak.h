#pragma once
#include<iostream>
#include<string> 
#include<vector>
#include"Pokemon.h"
#include"PokemonTrainer.h"

using namespace std; 

class ProfessorOak {

public:
	ProfessorOak();
	void askTrainerName(PokeTrainer* trainer);
	void giveStarterPokemon(PokeTrainer* trainer);

private: 

	string name;
	Pokemon* squirtle = new Pokemon("Squirtle", 100, 10, 5);
	Pokemon* bulbasaur = new Pokemon("Bulbasaur", 100, 10, 5);
	Pokemon* charmander = new Pokemon("Charmander", 100, 10, 5);

};