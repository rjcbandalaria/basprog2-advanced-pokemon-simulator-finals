#pragma once

#include<iostream>
#include<string>
#include<vector>
#include"Pokemon.h"
#include"Movement.h"

using namespace std; 

class PokeTrainer {

public:
	PokeTrainer();
	void getPokemon(Pokemon* pokemon);
	//void catchPokemon(Pokemon* pokemon);
	void playerMove(char direction);
	void checkPokemonStats();
	void displayStats(); 
	Pokemon* chooseBattlePokemon();
	~PokeTrainer();
	

	string name;
	vector<Pokemon*>* myPokemon= new vector<Pokemon*>();
	Position playerPosition; 
	bool isTrainerSafe;

};
