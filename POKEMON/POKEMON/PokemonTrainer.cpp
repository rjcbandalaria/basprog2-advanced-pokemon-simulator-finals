#include"PokemonTrainer.h"
#include"Pokemon.h"
#include"Movement.h"
#include"Map.h"
#include<string>
#include<iostream>
#include<vector>

using namespace std;

PokeTrainer::PokeTrainer()
{
	this->name = ""; 
	this->playerPosition.x = 0;
	this->playerPosition.y = 0;
	vector<Pokemon*>* myPokemon = new vector<Pokemon*>();
	//vector<PokeItem*>* inventory;

}

void PokeTrainer::getPokemon(Pokemon* pokemon)
{
		myPokemon->push_back(pokemon);
}


void PokeTrainer::playerMove(char direction)
{
	switch (direction) {
		case 'W':
		case 'w':
		playerPosition.y += 1;
		break; 
		
		case 'A':
		case'a':
			playerPosition.x -= 1;
			break;

		case 'S':
		case 's': 
			playerPosition.y -= 1;
			break;

		case 'D': 
		case 'd':
			playerPosition.x += 1;
			break;
	}
	cout << "(" << playerPosition.x << " , " << playerPosition.y << ")" << endl;
	system("pause");
	system("cls");
}

void PokeTrainer::checkPokemonStats()
{
	for (int i = 0; i < myPokemon->size(); i++) {
		myPokemon->at(i)->displayStats();
		cout << endl; 
	}
	system("pause");
	system("cls");
}

void PokeTrainer::displayStats()
{
	cout << "Name: " << this->name << endl; 
	cout << "Pokemons:" << endl; 
	for (int i = 0; i < myPokemon->size(); i++) {

		cout << myPokemon->at(i)->name << endl; 

	}
	system("pause");
	system("cls");
}

Pokemon* PokeTrainer::chooseBattlePokemon()
{
	for (int i = 0; i < myPokemon->size(); i++) {

		if (myPokemon->at(i)->hp > 0) {
			cout << myPokemon->at(i)->name << " , I choose you" << endl;
			return myPokemon->at(i);
			break;
		}

	}
	return myPokemon->at(0);
}

PokeTrainer::~PokeTrainer()
{
	delete[] myPokemon;
	myPokemon = NULL;
}






