#include "Pokemon.h"
#include<iostream>
#include<string>

using namespace std; 

Pokemon::Pokemon()
{
	this->name = "";
	this->baseHP = 100; 
	this->baseDamage = 10;
	this->level = 0;
	this->exp = 0;
	
	this->hp = initializePokemonHP();
	this->damage = initializePokemonDamage(); 

}

Pokemon::Pokemon(string name, int baseHp, int baseDamage)
{
	this->name = name; 
	this->baseHP = baseHp;
	this->baseDamage = baseDamage;
	this->level = initializePokemonLevel();//(rand() % 10) + 1;
	this->exp = 0;
	this->expLevelUp = initializePokemonExpLevelUp();

	this->hp = initializePokemonHP();//baseHP + (baseHP * (.15 * this->level));
	this->damage = initializePokemonDamage(); //baseDamage + (baseDamage * (.10 * this->level));
}


Pokemon::Pokemon(string name, int baseHP, int baseDamage, int level)
{
	this->name = name; 
	this->baseHP = baseHP;
	this->baseDamage = baseDamage;
	this->level = level; 
	this->exp = 0;
	this->expLevelUp = initializePokemonExpLevelUp();

	this->hp = initializePokemonHP(); //baseHP + (baseHP * (.15 * this->level));
	this->damage = initializePokemonDamage();// baseDamage + (baseDamage * (.10 * this->level));
}

void Pokemon::attackPokemon(Pokemon* enemy)
{
	enemy->hp = enemy->hp - this->damage;
	cout << this->name << " attacked " << enemy->name << endl; 
	cout << "Damage Inflicted: " << this->damage << endl; 
	cout << endl; 
	//enemy->displayStats();
	/*system("pause");
	system("cls");*/

}

void Pokemon::gainExp()
{
	this->exp = this->exp + (20 * this->level);
	if (this->exp >= this->expLevelUp) {
		this->levelUp();
	}
}

void Pokemon::levelUp()
{

	this->level += 1;
	this->hp = initializePokemonHP();
	this->damage = initializePokemonDamage();
	this->expLevelUp = initializePokemonExpLevelUp();
	this->exp = 0;


}

void Pokemon::displayStats()
{
	cout << "Name: " << this->name << endl; 
	cout << "Level: " << this->level << endl;
	cout << "HP: " << this->hp << endl; 
	cout << "Damage: " << this->damage << endl;
	cout << "Exp Points: " << this->exp << endl;
}

bool Pokemon::isDead()
{
	return this->hp<=0;
}

Pokemon::~Pokemon()
{
	
}






int Pokemon::initializePokemonHP()
{
	return this->hp = baseHP + (baseHP * (.15 * this->level));
}

int Pokemon::initializePokemonDamage()
{
	return baseDamage + (baseDamage * (.10 * this->level));
}

int Pokemon::initializePokemonLevel()
{
	return (rand()%10)+1;
}

int Pokemon::initializePokemonExpLevelUp()
{
	return this->expLevelUp + (50*this->level);
}



