#include "ProfessorOak.h"

ProfessorOak::ProfessorOak()
{
	this->name = "Professor Oak"; 
	Pokemon* squirtle = new Pokemon("Squirtle", 100, 10, 5);
	Pokemon* bulbasaur = new Pokemon("Bulbasaur", 100, 10, 5);
	Pokemon* charmander = new Pokemon("Charmander", 100, 10, 5);
}

void ProfessorOak::askTrainerName(PokeTrainer* trainer)
{
	cout << "Welcome Trainer! I am Professor Oak" << endl;
	cout << "What is your name?" << endl;
	cin >> trainer->name;
	cout << "Welcome " << trainer->name << "!" << " Welcome to the World of Pokemon" << endl;
}

void ProfessorOak::giveStarterPokemon(PokeTrainer* trainer)
{
	int choice;
	cout << "Choose your Pokemon: " << endl;
	cout << "1 " << "Bulbasaur " << endl;
	cout << "2 " << "Squirtle" << endl;
	cout << "3 " << "Charmander" << endl;

	cin >> choice;
	switch (choice) {
		
	case 1:
		trainer->getPokemon(bulbasaur);
		break;
	case 2:
		trainer->getPokemon(squirtle);
		break;

	case 3: 
		trainer->getPokemon(charmander);
		break; 

	}

	

}
