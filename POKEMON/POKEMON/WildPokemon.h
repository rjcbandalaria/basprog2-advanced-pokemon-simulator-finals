#pragma once
#include"Pokemon.h"
#include<iostream>
#include<string>
#include<vector>
#include"PokeDex.h"

using namespace std;

class WildPokemon {

public:
	void displayWildPokemon();
	void insertWildPokemon();

private:

	vector<PokeDex*>* wildPokemon = new vector<PokeDex*>();

};