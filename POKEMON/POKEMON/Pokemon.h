#pragma once
#include<iostream>
#include<string>
#include<time.h>

using namespace std;

class Pokemon{

public:
	Pokemon();
	Pokemon(string name, int baseHp, int baseDamage);
	Pokemon(string name, int baseHP, int baseDamage, int level);
	int initializePokemonHP();
	int initializePokemonDamage();
	int initializePokemonLevel();
	int initializePokemonExpLevelUp();
	void attackPokemon(Pokemon* enemy);
	void gainExp();
	void levelUp();
	void displayStats();
	bool isDead();
	~Pokemon();
	



	string name;
	int hp;
	int baseHP;
	int damage;
	int baseDamage;
	int exp;
	int expLevelUp;
	int level; 
	bool isCaptured;

};